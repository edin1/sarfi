from setuptools import setup

version = "0.1"

README = 'README'
long_description = open(README).read()

setup(name='sarfi',
      version=version,
      description=long_description,
      long_description=long_description,
      classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Topic :: Software Development :: Compilers',
        'Programming Language :: Python :: 2.6',
      ],
      keywords='peg parsing',
      author='Edin Salkovic',
      author_email='edin.salkovic@gmail.com',
      url='http://bitbucket.org/edin1/sarfi',
      license='GPL',
      requires=['mako'],
      packages=['sarfi'],
      zip_safe=False)
