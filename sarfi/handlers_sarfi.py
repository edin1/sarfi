import os

from sarfi import ast_helpers
from sarfi.ast_helpers import keepcwd
from mako.template import Template

# This is used to track already defined NonTerminals/Identifiers
IMPORTS = {}

# counters
_M = 0 # matches (a match may be true/false)
_I = 0 # indices
PN = 0

HEADER = """import os
import sys
import codecs

sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())

from sarfi import helpers
from sarfi import ast_helpers
from sarfi.ast_helpers import keepcwd

_h = helpers.Helpers()

class Parser(object):
    def __init__(self):
        self._r = ""
        self._newlines = []
        self._stack = []

"""
FOOTER = r"""

class Processor(object):
    def __init__(self, s, handlers):
        self.s = s
        self.handlers = handlers

    def process(self, node):
        if node:
            if isinstance(node, ast_helpers.AST):
                self.process_ast(node)
            elif isinstance(node, list):
                self.process_list(node)
            elif isinstance(node, str):
                pass
                # return self.process_string(node)
            else:
                raise NotImplementedError
        else:
            pass
            # raise Exception

    def process_ast(self, node):
        if isinstance(node.children, list):
            for child in node.children:
                self.process(child)
            node.value = self.list_to_value(node.children)
        elif isinstance(node.children, str):
            node.value = node.children
        elif isinstance(node.children, ast_helpers.AST):
            self.process(node.children)
            node.value = node.children.value
        elif node.children is None:
            node.value = ""
        else:
            raise Exception
        if hasattr(self.handlers, node.name):
            self.handle_handler(node, getattr(self.handlers, node.name))

    def process_list(self, node):
        for _node in node:
            self.process(_node)

    def handle_handler(self, node, handler):
        handler(node)

    def list_to_value(self, _list):
        return ast_helpers.list_to_text(_list)

    # def process_default(self, node):
    #     return self.s[node[2]:node[3]]

class Compiler(object):
    def __init__(self, grammar, handlers):
        self.handlers = handlers
        self.processor = Processor(grammar, self.handlers)

    def compile(self, node):
        return self.processor.process(node)

def main():
    import io
    from optparse import OptionParser
    from pprint import pprint

    cli_parser = OptionParser(
        usage='usage: %prog [options] <grammar.file>'
        )
    cli_parser.add_option("-d", "--debug", action="store_true",
                      dest="debug", default=False,
                      help="Enable debugging")
    cli_parser.add_option("--handlers", dest="handlers",
                      help="Get handlers from FILE", metavar="FILE")
    (options, args) = cli_parser.parse_args()

    try:
        grammar_file = args[0]
    except IndexError:
        cli_parser.error("You have to supply a PEG <grammar.file>")
    grammar = io.open(grammar_file, "r", encoding="utf-8").read()
    #print grammar

    if not options.handlers:
        Handlers = object
    else:
        handlers = dict()
        exec(compile(open(options.handlers).read(), options.handlers, 'exec'),
             handlers)
        handlers["_DEBUG"] = options.debug
        if "Handlers" in handlers:
            Handlers = handlers["Handlers"]
        else:
            cli_parser.error("Your handlers file doesn't have a Handlers class")
    with keepcwd():
        dirname = os.path.dirname(grammar_file)
        if dirname:
            os.chdir(dirname)
        compiler = Compiler(grammar, Handlers())
        parser = Parser()
        result = parse(grammar, parser, compiler)
    if result[0]:
        print(result[1])
    else:
        cli_parser.error(result[1])

def parse(grammar, parser, compiler, filename=None):
    import sys
    import bisect
    import time

    parser = Parser()
    t1 = time.time()
    node = parser.{0}(grammar, 0)
    name, isparsed, zero, length, ast = node
    t2 = time.time()
    if not isparsed or (length != len(grammar)):
        if filename is not None:
            message = 'The file "%s" is not a valid PEG file.'%filename
        else:
            message = "You have to supply a PEG <grammar.file>"
        _i = parser._stack[0][1]
        _name = parser._stack[0][0]
        _line_no = bisect.bisect(parser._newlines, _i) - 1
        if _line_no != -1:
            _newline = parser._newlines[_line_no]
        else:
            _newline = -1
        _next_newline = -1
        for _c in ('\r\n', '\n', '\r'):
            _c_i = grammar.find(_c, _newline+1)
            if  _c_i != -1:
                if _next_newline != -1:
                    if _c_i > _next_newline:
                        continue
                _next_newline = _c_i
        message += "Error while parsing line %d:\n"%(_line_no+1)
        if _next_newline != -1:
            _line = grammar[_newline+1:_next_newline]
        else:
            _line = grammar[_newline+1:]
        message += '%s\n'%_line
        _pointing_line = " "*(_i - _newline - 1) + "^\n"
        message += _pointing_line
        #message += repr(parser._stack) + "\n"
        message += "Traceback:\n"
        for _name, _j in reversed(parser._stack):
            if _j < _i:
                pass
                #message += "%s, %d\n"%(_name, _j)
            if _j == _i:
                if _i == len(grammar):
                    got = "EOF"
                else:
                    got = '"%s"'%grammar[_i]
                message += 'Expected %s at %d (%d, %d), got %s\n\n'%(_name, _i, _line_no + 1, (_i - _newline - 1), got)
                break
        return (False, message)
    else:
        t3 = time.time()
        node = ast_helpers.helper_remover(node)
        node = ast_helpers.node2ast(node)
        compiler.compile(node)
        result = node.value
        t4 = time.time()
        t5 = time.time()
        # print "The parsing took", str(t2-t1), "seconds."
        # print "Removing the helpers took", str(t4-t3), "seconds."
        # print "Converting to an AST object took", str(t5-t4), "seconds."
        # print "ast = ",
        # pprint(node)
        # result = ast_helpers.ast2dot(node)
        # pprint (ast)
        return (True, result)

if __name__ == '__main__':
    import pdb
    main()
    # import profile
    # profile.run('main()')
"""

def _f(template, *args, **kwargs):
    _globals = globals()
    for key in _globals:
        if not key not in kwargs:
            kwargs[key] = _globals[key]
    kwargs["args"] = args
    kwargs["split"] = _split
    kwargs["strip"] = _strip
    return Template(template).render(**kwargs)

def get_prefix(filename):
    global IMPORTS
    global PN
    abspath = os.path.abspath(filename)
    if abspath in IMPORTS:
        return IMPORTS[abspath]
    else:
        IMPORTS[abspath] = leavealnum(os.path.basename(filename)) + str(PN) + "_"
        PN += 1
        return IMPORTS[abspath]

def leavealnum(text):
    res = ""
    for letter in text:
        if letter.isalnum():
            res += letter
    return res

def _fix_indent(tokens):
    """Used for indenting"""
    return "".join(tokens).split("\n")

def _split(s):
    return s.split("\n")

def _strip(s):
    return s.strip()

def child_to_text(child):
    if isinstance(child, list):
        return ast_helpers.list_to_text(child)
    elif isinstance(child, ast_helpers.AST):
        return child.value
    elif isinstance(child, str):
        return child
    else:
        raise NotImplementedError

def range_escape(text):
    text = text.replace('\[', '[')
    text = text.replace('\]', ']')
    text = text.replace('"', '\\"')
    return text

class Handlers(object):
    def __init__(self, header=HEADER,
                 footer=FOOTER, imports="", prefix="", defs=None):
        self._header = header
        self._footer = footer
        self._imports = imports
        self._prefix = prefix
        if defs is None:
            self._defs = list()
        else:
            self._defs = defs

    def Grammar(self, node):
        tokens = [node.children[1]]
        for _items in node.children[2]:
            if _items[1]:
                tokens.append(_items[1])
        text = self._header
        text += _f(
"""
% for _def in tokens:
    % for line in split(_def.value):
        % if strip(line):
    ${line}
        % else:

        % endif
    % endfor
% endfor

% for line in split(imports):
    % if strip(line):
${line}
    % else:

    % endif
% endfor
""", tokens=tokens, imports=self._imports)
        text += self._footer.format(self._defs[0][5:])
        node.value = text

    def Definition(self, node):
        tokens = node.children
        NonTerminal = tokens[0].value
        if NonTerminal in self._defs:
            raise Exception("%s is defined more than once!"%NonTerminal)
        self._defs.append(NonTerminal)
        tokens = "".join(tokens[2].value)
        if tokens.startswith("self."):
            result_text = "result"
        else:
            result_text = "children"
        text = _f(
'''
def ${NT}(self, _s, _i0):
    result = ${tokens}(_s, _i0)
    name, _m, _i0, _i, children = result
    if _m:
        if self._stack and _i0 > self._stack[0][1]:
            self._stack = []
        % if NT == "EndOfLine":
        self._newlines.append(_i0)
        % endif
        return "ID_${NT}", True, _i0, _i, ${result_text}
    else:
        if self._stack and _i0 > self._stack[0][1]:
            self._stack = []
        self._stack.append(("${NT}", _i0))
        return "ID_${NT}", False, _i0, _i0, ${result_text}
''',
        NT=NonTerminal[5:], tokens=tokens, result_text=result_text)
        node.value = text

    def Expression(self, node):
        tokens = [node.children[0].value] + [token[1].value for token in node.children[1] if token]
        if len(tokens) == 1:
            node.value = "".join(tokens)
            return
        # else:
        text = _f("_h.Expression([${tokens}])", tokens=", ".join(tokens))
        node.value = text

    def Sequence(self, node):
        tokens = [node.children[1].value]
        for pair in node.children[2]:
            if pair[1]:
                tokens.append(pair[1].value)
        if len(tokens) == 1:
            node.value = "".join(tokens)
            return
        # else:
        text = _f(
    """_h.Sequence([${tokens}])""", tokens=", ".join(tokens))
        node.value = text

    def Set(self, node):
        tokens = [node.children[1].value] + [token[1].value for token in node.children[2] if token]
        if len(tokens) == 1:
            node.value = "".join(tokens)
            return
        # else:
        text = _f("_h.Expression([${tokens}])", tokens=", ".join(tokens))
        node.value = text

    def Prefix(self, node):
        tokens = node.children[0]
        if tokens:
            if tokens.name == "AND":
                node.value = "_h.AND(" + node.children[1].value.rstrip() + ")"
            elif tokens.name == "NOT":
                node.value = "_h.NOT(" + node.children[1].value.rstrip() + ")"
        else:
            node.value = node.children[1].value

    def Suffix(self, node):
        primary = node.children[0]
        tokens = node.children[1]
        if not tokens:
            node.value = primary.value
        else:
            if tokens.name == "QUESTION":
                node.value = "_h.ZeroOrOne(" + primary.value + ")"
            elif tokens.name == "STAR":
                node.value = "_h.ZeroOrMore(" + primary.value + ")"
            elif tokens.name == "PLUS":
                node.value = "_h.OneOrMore(" + primary.value + ")"
            else:
                raise Exception
        if node.children[2]:
            node.value = "_h.MatchAndDump(" + node.value + ")"

    def Primary(self, node):
        primary = node.children#[0]
        if isinstance(primary, list):
            if len(primary) == 1: # Identifier?
                node.value = primary[0].value
            elif len(primary) == 3: # Expression?
                node.value = primary[1].value
        elif isinstance(primary, ast_helpers.AST):
            if primary.name == "Literal":
                string = primary.value
                if len(string) > 1:
                    # remove quoting characters
                    string = string[1:][:-1]
                node.value = '_h.Literal("' + string.replace('"', '\\"') + '")'
            elif primary.name == "Class":
                node.value = primary.value
            elif primary.name == "DOT":
                node.value = "_h.Dot()"
            elif primary.name == "Import":
                from sarfi import parser

                import_name = node.children.children[0].value[5:]#.split("_", 1)[1]
                filename = node.children.children[2].value
                grammar = open(filename).read()

                abspath = os.path.abspath(filename)

                if abspath not in IMPORTS:
                    prefix = get_prefix(filename)
                    with keepcwd():
                        dirname = os.path.dirname(filename)
                        if dirname:
                            os.chdir(dirname)
                        compiler = parser.Compiler(grammar, HandlersImport(prefix=prefix))
                        parser_ = parser.Parser()
                        result = parser.parse(grammar, parser_, compiler)

                    if result[0]:
                        self._imports += result[1]
                    else:
                        raise Exception(result[1])
                else:
                    prefix = get_prefix(filename)
                node.value = "%s()."%prefix + import_name
        else:
            raise Exception

    def Class(self, node):
        tokens = node.children[1]
        text = "_h.Expression(["
        for _range in tokens[:-1]:
            text += _range[1].value + ", "
        node.value =  text + tokens[-1][1].value + "])"

    def Identifier(self, node):
        ident = node.value
        node.value = "self." + ident

    def Range(self, node):
        tokens = node.children
        if isinstance(tokens, ast_helpers.AST):
            node.value = '_h.Range(["' + range_escape(tokens.value) + '"])'
            #node.value = '_h.Literal("' + range_escape(node.value) + '")'
        elif isinstance(tokens, list):
            node.value = '_h.Range(["' + range_escape(tokens[0].value) + '", "' + range_escape(tokens[2].value) + '"])'
            # text = ""
            # ord1 = ord(tokens[0].value)
            # ord2 = ord(tokens[2].value)
            # for i in range(ord1, ord2+1):
            #     text += chr(i)
            # node.value = text
        else:
            raise Exception

    def UnicodeEscapeSequence(self, node):
        node.value = eval("'" + node.value + "'")

#     def LEFTARROW(self, node):
#         return "".join(node.children)

    def SLASH(self, node):
        node.value = ""

    def Spacing(self, node):
        node.value = ""

    def CLOSE(self, node):
        node.value = ""

    def OPEN(self, node):
        node.value = ""

class HandlersImport(Handlers):
    def Grammar(self, node):
        tokens = [node.children[1]]
        for _items in node.children[2]:
            if _items[1]:
                tokens.append(_items[1])
        text = _f(
"""
class ${prefix}(object):
    def __init__(self):
        self._r = ""
        self._newlines = []
        self._stack = []

% for _def in tokens:
    % for line in split(_def.value):
        % if strip(line):
    ${line}
        % else:

        % endif
    % endfor
% endfor

% for line in split(imports):
    % if strip(line):
${line}
    % else:

    % endif
% endfor
""", tokens=tokens, prefix=self._prefix, imports=self._imports)
        node.value = text
