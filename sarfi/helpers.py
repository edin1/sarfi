def Helpers():
    return HelpersProxy(_Helpers())
    #return _Helpers()

class HelpersProxy(object):
    def __init__(self, helpers):
        self._helpers = helpers

    def __getattr__(self, name):
        f = getattr(self._helpers, name)
        #print f, dir(f)
        def _f(*args, **kwargs):
            def __f(s, i0):
                m, i, children = f(*args, **kwargs)(s, i0)
                return "H_" + name, m, i0, i, children
            return __f
        return _f


class _Helpers(object):
    def Expression(self, sequences):
        def _f(s, i0):
            i = i0
            for f in sequences:
                name, m, _i0, i, children = f(s, i)
                if m:
                    return True, i, (name, m, _i0, i, children)
            return False, i0, None
        return _f

    def Sequence(self, prefixes):
        def _f(s, i0):
            i = i0
            ismatched = False
            children = []
            for f in prefixes:
                name, m, _i0, i, _children = f(s, i)
                if not m:
                    return False, i0, None
                children.append((name, m, _i0, i, _children))
            return True, i, children
        return _f

    def AND(self, suffix):
        def _f(s, i0):
            i = i0
            name, m, i0, i, _children = suffix(s, i)
            if m:
                return True, i0, [(name, m, i0, i, _children)]
            else:
                return False, i0, None
        return _f

    def NOT(self, suffix):
        def _f(s, i0):
            i = i0
            name, m, i0, i, _children = suffix(s, i)
            if not m:
                return True, i0, [(name, m, i0, i, _children)]
            else:
                return False, i0, None
        return _f

    def ZeroOrOne(self, primary):
        def _f(s, i0):
            i = i0
            name, m, i0, i, _children = primary(s, i)
            if m:
                return True, i, (name, m, i0, i, _children)
            else:
                return True, i0, None
        return _f

    def ZeroOrMore(self, primary):
        def _f(s, i0):
            i = i0
            children = []
            while True:
                name, m, i0, i, _children = primary(s, i)
                if not m:
                    return True, i, children
                else:
                    children.append((name, m, i0, i, _children))
        return _f

    def OneOrMore(self, primary):
        def _f(s, i0):
            i = i0
            name, m, i0, i, _children = primary(s, i)
            if not m:
                return False, i0, None
            else:
                children = [(name, m, i0, i, _children)]
            while True:
                name, m, i0, i, _children = primary(s, i)
                if not m:
                    return True, i, children
                else:
                    children.append((name, m, i0, i, _children))
        return _f

    def Literal(self, string):
        def _f(s, i0):
            i = i0 + len(string)
            if s[i0:i] == string:
                return True, i, string
            else:
                return False, i0, None
        return _f

    def Range(self, chars):
        def _f(s, i0):
            try:
                nextchar = s[i0]
            except IndexError:
                return False, i0, None
            if len(chars) == 1:
                if chars[0] == nextchar:
                    return True, i0 + 1, nextchar
                # else:
                return False, i0, None
            # else len(chars) == 1:
            if chars[0] <= nextchar <= chars[1]:
                return True, i0 + 1, nextchar
            # else:
            return False, i0, None
        return _f

    def Dot(self):
        def _f(s, i0):
            try:
                nextchar = s[i0]
            except IndexError:
                return False, i0, None
            return True, i0 + 1, nextchar
        return _f

    def MatchAndDump(self, suffix):
        def _f(s, i0):
            i = i0
            name, m, i0, i, _children = suffix(s, i)
            if m:
                return True, i, None
            else:
                return False, i0, None
        return _f
