import os
import sys
import codecs

sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())

from sarfi import helpers
from sarfi import ast_helpers
from sarfi.ast_helpers import keepcwd

_h = helpers.Helpers()

class Parser(object):
    def __init__(self):
        self._r = ""
        self._newlines = []
        self._stack = []



    def Grammar(self, _s, _i0):
        result = _h.Sequence([_h.ZeroOrMore(_h.Expression([self.Comment, self.EndOfLine])), self.Definition, _h.ZeroOrMore(_h.Sequence([_h.OneOrMore(_h.Expression([self.Comment, self.EndOfLine])), self.Definition])), _h.ZeroOrMore(_h.Expression([self.Comment, self.EndOfLine]))])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Grammar", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Grammar", _i0))
            return "ID_Grammar", False, _i0, _i0, children


    def Definition(self, _s, _i0):
        result = _h.Sequence([self.Identifier, _h.Literal(": "), self.Prefix, _h.ZeroOrOne(_h.Sequence([self.EndOfLine, self.Commands, _h.ZeroOrMore(_h.Sequence([self.EndOfLine, self.Commands]))]))])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Definition", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Definition", _i0))
            return "ID_Definition", False, _i0, _i0, children


    def InlineDefinition(self, _s, _i0):
        result = _h.Sequence([self.Identifier, _h.Literal(": "), self.Prefix])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_InlineDefinition", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("InlineDefinition", _i0))
            return "ID_InlineDefinition", False, _i0, _i0, children


    def Commands(self, _s, _i0):
        result = _h.Expression([self.Color, self.BGColor, self.Python])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Commands", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Commands", _i0))
            return "ID_Commands", False, _i0, _i0, children


    def Color(self, _s, _i0):
        result = _h.Sequence([_h.Literal("\\color{"), self.ColorName, _h.Literal("}")])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Color", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Color", _i0))
            return "ID_Color", False, _i0, _i0, children


    def BGColor(self, _s, _i0):
        result = _h.Sequence([_h.Literal("\\bg{"), self.ColorName, _h.Literal("}")])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_BGColor", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("BGColor", _i0))
            return "ID_BGColor", False, _i0, _i0, children


    def Python(self, _s, _i0):
        result = _h.Sequence([_h.Literal("\\python{"), self.PythonCode, _h.Literal("}")])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Python", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Python", _i0))
            return "ID_Python", False, _i0, _i0, children


    def PythonCode(self, _s, _i0):
        result = PythonMocksarfi0_().Grammar(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_PythonCode", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("PythonCode", _i0))
            return "ID_PythonCode", False, _i0, _i0, children


    def ColorName(self, _s, _i0):
        result = _h.OneOrMore(_h.Expression([_h.Range(["a", "z"]), _h.Range(["A", "Z"]), _h.Range(["_"]), _h.Range(["0", "9"]), _h.Range(["-"])]))(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_ColorName", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("ColorName", _i0))
            return "ID_ColorName", False, _i0, _i0, children


    def Set(self, _s, _i0):
        result = _h.Sequence([_h.Literal("{"), self.Prefix, _h.OneOrMore(_h.Sequence([_h.Literal(", "), self.Prefix])), _h.Literal("}")])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Set", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Set", _i0))
            return "ID_Set", False, _i0, _i0, children


    def Sequence(self, _s, _i0):
        result = _h.Sequence([_h.Literal("("), self.Prefix, _h.ZeroOrMore(_h.Sequence([_h.Literal(", "), self.Prefix])), _h.Literal(")")])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Sequence", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Sequence", _i0))
            return "ID_Sequence", False, _i0, _i0, children


    def Prefix(self, _s, _i0):
        result = _h.Sequence([_h.ZeroOrOne(_h.Expression([self.AND, self.NOT])), self.Suffix, _h.ZeroOrOne(self.InlineCommands)])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Prefix", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Prefix", _i0))
            return "ID_Prefix", False, _i0, _i0, children


    def Suffix(self, _s, _i0):
        result = _h.Sequence([self.Primary, _h.ZeroOrOne(_h.Expression([self.QUESTION, self.STAR, self.PLUS])), _h.ZeroOrOne(_h.Literal("!"))])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Suffix", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Suffix", _i0))
            return "ID_Suffix", False, _i0, _i0, children


    def Primary(self, _s, _i0):
        result = _h.Expression([self.Import, self.SetOp, self.Identifier, self.Set, self.Sequence, self.Literal, self.Class, self.DOT])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Primary", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Primary", _i0))
            return "ID_Primary", False, _i0, _i0, children


    def Import(self, _s, _i0):
        result = _h.Sequence([self.Identifier, _h.Sequence([_h.Literal(" "), _h.Literal("from"), _h.Literal(" ")]), self.FileURL])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Import", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Import", _i0))
            return "ID_Import", False, _i0, _i0, children


    def FileURL(self, _s, _i0):
        result = _h.Sequence([_h.ZeroOrOne(self.FileName), _h.ZeroOrMore(_h.Sequence([_h.Literal("/"), self.FileName]))])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_FileURL", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("FileURL", _i0))
            return "ID_FileURL", False, _i0, _i0, children


    def FileName(self, _s, _i0):
        result = _h.ZeroOrMore(_h.Sequence([_h.NOT(_h.Expression([_h.Literal("/"), self.EndOfLine])), _h.Dot()]))(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_FileName", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("FileName", _i0))
            return "ID_FileName", False, _i0, _i0, children


    def SetOp(self, _s, _i0):
        result = _h.Sequence([self.Identifier, _h.Literal("("), self.Set, _h.Literal(")")])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_SetOp", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("SetOp", _i0))
            return "ID_SetOp", False, _i0, _i0, children


    def InlineCommands(self, _s, _i0):
        result = _h.ZeroOrMore(_h.Expression([self.Color, self.BGColor]))(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_InlineCommands", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("InlineCommands", _i0))
            return "ID_InlineCommands", False, _i0, _i0, children


    def Identifier(self, _s, _i0):
        result = _h.Sequence([self.IdentStart, _h.ZeroOrMore(self.IdentCont)])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Identifier", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Identifier", _i0))
            return "ID_Identifier", False, _i0, _i0, children


    def IdentStart(self, _s, _i0):
        result = _h.Expression([_h.Range(["a", "z"]), _h.Range(["A", "Z"]), _h.Range(["_"])])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_IdentStart", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("IdentStart", _i0))
            return "ID_IdentStart", False, _i0, _i0, children


    def IdentCont(self, _s, _i0):
        result = _h.Expression([self.IdentStart, _h.Expression([_h.Range(["0", "9"])])])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_IdentCont", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("IdentCont", _i0))
            return "ID_IdentCont", False, _i0, _i0, children


    def Literal(self, _s, _i0):
        result = _h.Expression([_h.Sequence([_h.Expression([_h.Range(["'"])]), _h.OneOrMore(_h.Sequence([_h.NOT(_h.Expression([_h.Range(["'"])])), self.Char])), _h.Expression([_h.Range(["'"])])]), _h.Sequence([_h.Expression([_h.Range(["\""])]), _h.OneOrMore(_h.Sequence([_h.NOT(_h.Expression([_h.Range(["\""])])), self.Char])), _h.Expression([_h.Range(["\""])])])])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Literal", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Literal", _i0))
            return "ID_Literal", False, _i0, _i0, children


    def Class(self, _s, _i0):
        result = _h.Sequence([_h.Literal("["), _h.OneOrMore(_h.Sequence([_h.NOT(_h.Literal("]")), self.Range])), _h.Literal("]")])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Class", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Class", _i0))
            return "ID_Class", False, _i0, _i0, children


    def Range(self, _s, _i0):
        result = _h.Expression([_h.Sequence([self.Char, _h.Literal("-"), self.Char]), self.Char])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Range", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Range", _i0))
            return "ID_Range", False, _i0, _i0, children


    def Char(self, _s, _i0):
        result = _h.Expression([self.UnicodeEscapeSequence, _h.Sequence([_h.Literal("\\"), _h.Expression([_h.Range(["n"]), _h.Range(["r"]), _h.Range(["t"]), _h.Range(["'"]), _h.Range(["\""]), _h.Range(["["]), _h.Range(["]"]), _h.Range(["\\"])])]), _h.Sequence([_h.Literal("\\"), _h.Expression([_h.Range(["0", "2"])]), _h.Expression([_h.Range(["0", "7"])]), _h.Expression([_h.Range(["0", "7"])])]), _h.Sequence([_h.Literal("\\"), _h.Expression([_h.Range(["0", "7"])]), _h.ZeroOrOne(_h.Expression([_h.Range(["0", "7"])]))]), _h.Sequence([_h.NOT(_h.Literal("\\")), _h.Dot()])])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Char", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Char", _i0))
            return "ID_Char", False, _i0, _i0, children


    def UnicodeEscapeSequence(self, _s, _i0):
        result = _h.Sequence([_h.Literal("\\u"), self.HEX, self.HEX, self.HEX, self.HEX])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_UnicodeEscapeSequence", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("UnicodeEscapeSequence", _i0))
            return "ID_UnicodeEscapeSequence", False, _i0, _i0, children


    def HEX(self, _s, _i0):
        result = _h.Expression([_h.Range(["0", "9"]), _h.Range(["a", "f"]), _h.Range(["A", "F"])])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_HEX", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("HEX", _i0))
            return "ID_HEX", False, _i0, _i0, children


    def AND(self, _s, _i0):
        result = _h.Literal("&")(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_AND", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("AND", _i0))
            return "ID_AND", False, _i0, _i0, children


    def NOT(self, _s, _i0):
        result = _h.Literal("!")(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_NOT", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("NOT", _i0))
            return "ID_NOT", False, _i0, _i0, children


    def QUESTION(self, _s, _i0):
        result = _h.Literal("?")(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_QUESTION", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("QUESTION", _i0))
            return "ID_QUESTION", False, _i0, _i0, children


    def STAR(self, _s, _i0):
        result = _h.Literal("*")(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_STAR", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("STAR", _i0))
            return "ID_STAR", False, _i0, _i0, children


    def PLUS(self, _s, _i0):
        result = _h.Literal("+")(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_PLUS", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("PLUS", _i0))
            return "ID_PLUS", False, _i0, _i0, children


    def DOT(self, _s, _i0):
        result = _h.Literal(".")(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_DOT", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("DOT", _i0))
            return "ID_DOT", False, _i0, _i0, children


    def Spacing(self, _s, _i0):
        result = _h.ZeroOrMore(_h.Expression([self.Space, self.Comment]))(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Spacing", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Spacing", _i0))
            return "ID_Spacing", False, _i0, _i0, children


    def Comment(self, _s, _i0):
        result = _h.Sequence([_h.Literal("#"), _h.ZeroOrMore(_h.Sequence([_h.NOT(self.EndOfLine), _h.Dot()])), self.EndOfLine])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Comment", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Comment", _i0))
            return "ID_Comment", False, _i0, _i0, children


    def Space(self, _s, _i0):
        result = _h.Expression([_h.Literal(" "), _h.Literal("\t"), self.EndOfLine])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Space", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Space", _i0))
            return "ID_Space", False, _i0, _i0, children


    def EndOfLine(self, _s, _i0):
        result = _h.Expression([_h.Sequence([_h.Literal("\r"), _h.ZeroOrOne(_h.Literal("\n"))]), _h.Literal("\n")])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._newlines.append(_i0)
            return "ID_EndOfLine", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("EndOfLine", _i0))
            return "ID_EndOfLine", False, _i0, _i0, children



class PythonMocksarfi0_(object):
    def __init__(self):
        self._r = ""
        self._newlines = []
        self._stack = []


    def Grammar(self, _s, _i0):
        result = self.Identifier(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Grammar", True, _i0, _i, result
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Grammar", _i0))
            return "ID_Grammar", False, _i0, _i0, result


    def Identifier(self, _s, _i0):
        result = sarfisarfi1_().Identifier(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Identifier", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Identifier", _i0))
            return "ID_Identifier", False, _i0, _i0, children



class sarfisarfi1_(object):
    def __init__(self):
        self._r = ""
        self._newlines = []
        self._stack = []


    def Grammar(self, _s, _i0):
        result = _h.Sequence([_h.ZeroOrMore(_h.Expression([self.Comment, self.EndOfLine])), self.Definition, _h.ZeroOrMore(_h.Sequence([_h.OneOrMore(_h.Expression([self.Comment, self.EndOfLine])), self.Definition])), _h.ZeroOrMore(_h.Expression([self.Comment, self.EndOfLine]))])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Grammar", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Grammar", _i0))
            return "ID_Grammar", False, _i0, _i0, children


    def Definition(self, _s, _i0):
        result = _h.Sequence([self.Identifier, _h.Literal(": "), self.Prefix, _h.ZeroOrOne(_h.Sequence([self.EndOfLine, self.Commands, _h.ZeroOrMore(_h.Sequence([self.EndOfLine, self.Commands]))]))])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Definition", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Definition", _i0))
            return "ID_Definition", False, _i0, _i0, children


    def InlineDefinition(self, _s, _i0):
        result = _h.Sequence([self.Identifier, _h.Literal(": "), self.Prefix])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_InlineDefinition", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("InlineDefinition", _i0))
            return "ID_InlineDefinition", False, _i0, _i0, children


    def Commands(self, _s, _i0):
        result = _h.Expression([self.Color, self.BGColor, self.Python])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Commands", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Commands", _i0))
            return "ID_Commands", False, _i0, _i0, children


    def Color(self, _s, _i0):
        result = _h.Sequence([_h.Literal("\\color{"), self.ColorName, _h.Literal("}")])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Color", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Color", _i0))
            return "ID_Color", False, _i0, _i0, children


    def BGColor(self, _s, _i0):
        result = _h.Sequence([_h.Literal("\\bg{"), self.ColorName, _h.Literal("}")])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_BGColor", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("BGColor", _i0))
            return "ID_BGColor", False, _i0, _i0, children


    def Python(self, _s, _i0):
        result = _h.Sequence([_h.Literal("\\python{"), self.PythonCode, _h.Literal("}")])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Python", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Python", _i0))
            return "ID_Python", False, _i0, _i0, children


    def PythonCode(self, _s, _i0):
        result = PythonMocksarfi0_().Grammar(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_PythonCode", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("PythonCode", _i0))
            return "ID_PythonCode", False, _i0, _i0, children


    def ColorName(self, _s, _i0):
        result = _h.OneOrMore(_h.Expression([_h.Range(["a", "z"]), _h.Range(["A", "Z"]), _h.Range(["_"]), _h.Range(["0", "9"]), _h.Range(["-"])]))(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_ColorName", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("ColorName", _i0))
            return "ID_ColorName", False, _i0, _i0, children


    def Set(self, _s, _i0):
        result = _h.Sequence([_h.Literal("{"), self.Prefix, _h.OneOrMore(_h.Sequence([_h.Literal(", "), self.Prefix])), _h.Literal("}")])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Set", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Set", _i0))
            return "ID_Set", False, _i0, _i0, children


    def Sequence(self, _s, _i0):
        result = _h.Sequence([_h.Literal("("), self.Prefix, _h.ZeroOrMore(_h.Sequence([_h.Literal(", "), self.Prefix])), _h.Literal(")")])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Sequence", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Sequence", _i0))
            return "ID_Sequence", False, _i0, _i0, children


    def Prefix(self, _s, _i0):
        result = _h.Sequence([_h.ZeroOrOne(_h.Expression([self.AND, self.NOT])), self.Suffix, _h.ZeroOrOne(self.InlineCommands)])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Prefix", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Prefix", _i0))
            return "ID_Prefix", False, _i0, _i0, children


    def Suffix(self, _s, _i0):
        result = _h.Sequence([self.Primary, _h.ZeroOrOne(_h.Expression([self.QUESTION, self.STAR, self.PLUS])), _h.ZeroOrOne(_h.Literal("!"))])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Suffix", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Suffix", _i0))
            return "ID_Suffix", False, _i0, _i0, children


    def Primary(self, _s, _i0):
        result = _h.Expression([self.Import, self.SetOp, self.Identifier, self.Set, self.Sequence, self.Literal, self.Class, self.DOT])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Primary", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Primary", _i0))
            return "ID_Primary", False, _i0, _i0, children


    def Import(self, _s, _i0):
        result = _h.Sequence([self.Identifier, _h.Sequence([_h.Literal(" "), _h.Literal("from"), _h.Literal(" ")]), self.FileURL])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Import", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Import", _i0))
            return "ID_Import", False, _i0, _i0, children


    def FileURL(self, _s, _i0):
        result = _h.Sequence([_h.ZeroOrOne(self.FileName), _h.ZeroOrMore(_h.Sequence([_h.Literal("/"), self.FileName]))])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_FileURL", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("FileURL", _i0))
            return "ID_FileURL", False, _i0, _i0, children


    def FileName(self, _s, _i0):
        result = _h.ZeroOrMore(_h.Sequence([_h.NOT(_h.Expression([_h.Literal("/"), self.EndOfLine])), _h.Dot()]))(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_FileName", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("FileName", _i0))
            return "ID_FileName", False, _i0, _i0, children


    def SetOp(self, _s, _i0):
        result = _h.Sequence([self.Identifier, _h.Literal("("), self.Set, _h.Literal(")")])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_SetOp", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("SetOp", _i0))
            return "ID_SetOp", False, _i0, _i0, children


    def InlineCommands(self, _s, _i0):
        result = _h.ZeroOrMore(_h.Expression([self.Color, self.BGColor]))(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_InlineCommands", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("InlineCommands", _i0))
            return "ID_InlineCommands", False, _i0, _i0, children


    def Identifier(self, _s, _i0):
        result = _h.Sequence([self.IdentStart, _h.ZeroOrMore(self.IdentCont)])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Identifier", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Identifier", _i0))
            return "ID_Identifier", False, _i0, _i0, children


    def IdentStart(self, _s, _i0):
        result = _h.Expression([_h.Range(["a", "z"]), _h.Range(["A", "Z"]), _h.Range(["_"])])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_IdentStart", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("IdentStart", _i0))
            return "ID_IdentStart", False, _i0, _i0, children


    def IdentCont(self, _s, _i0):
        result = _h.Expression([self.IdentStart, _h.Expression([_h.Range(["0", "9"])])])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_IdentCont", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("IdentCont", _i0))
            return "ID_IdentCont", False, _i0, _i0, children


    def Literal(self, _s, _i0):
        result = _h.Expression([_h.Sequence([_h.Expression([_h.Range(["'"])]), _h.OneOrMore(_h.Sequence([_h.NOT(_h.Expression([_h.Range(["'"])])), self.Char])), _h.Expression([_h.Range(["'"])])]), _h.Sequence([_h.Expression([_h.Range(["\""])]), _h.OneOrMore(_h.Sequence([_h.NOT(_h.Expression([_h.Range(["\""])])), self.Char])), _h.Expression([_h.Range(["\""])])])])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Literal", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Literal", _i0))
            return "ID_Literal", False, _i0, _i0, children


    def Class(self, _s, _i0):
        result = _h.Sequence([_h.Literal("["), _h.OneOrMore(_h.Sequence([_h.NOT(_h.Literal("]")), self.Range])), _h.Literal("]")])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Class", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Class", _i0))
            return "ID_Class", False, _i0, _i0, children


    def Range(self, _s, _i0):
        result = _h.Expression([_h.Sequence([self.Char, _h.Literal("-"), self.Char]), self.Char])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Range", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Range", _i0))
            return "ID_Range", False, _i0, _i0, children


    def Char(self, _s, _i0):
        result = _h.Expression([self.UnicodeEscapeSequence, _h.Sequence([_h.Literal("\\"), _h.Expression([_h.Range(["n"]), _h.Range(["r"]), _h.Range(["t"]), _h.Range(["'"]), _h.Range(["\""]), _h.Range(["["]), _h.Range(["]"]), _h.Range(["\\"])])]), _h.Sequence([_h.Literal("\\"), _h.Expression([_h.Range(["0", "2"])]), _h.Expression([_h.Range(["0", "7"])]), _h.Expression([_h.Range(["0", "7"])])]), _h.Sequence([_h.Literal("\\"), _h.Expression([_h.Range(["0", "7"])]), _h.ZeroOrOne(_h.Expression([_h.Range(["0", "7"])]))]), _h.Sequence([_h.NOT(_h.Literal("\\")), _h.Dot()])])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Char", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Char", _i0))
            return "ID_Char", False, _i0, _i0, children


    def UnicodeEscapeSequence(self, _s, _i0):
        result = _h.Sequence([_h.Literal("\\u"), self.HEX, self.HEX, self.HEX, self.HEX])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_UnicodeEscapeSequence", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("UnicodeEscapeSequence", _i0))
            return "ID_UnicodeEscapeSequence", False, _i0, _i0, children


    def HEX(self, _s, _i0):
        result = _h.Expression([_h.Range(["0", "9"]), _h.Range(["a", "f"]), _h.Range(["A", "F"])])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_HEX", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("HEX", _i0))
            return "ID_HEX", False, _i0, _i0, children


    def AND(self, _s, _i0):
        result = _h.Literal("&")(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_AND", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("AND", _i0))
            return "ID_AND", False, _i0, _i0, children


    def NOT(self, _s, _i0):
        result = _h.Literal("!")(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_NOT", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("NOT", _i0))
            return "ID_NOT", False, _i0, _i0, children


    def QUESTION(self, _s, _i0):
        result = _h.Literal("?")(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_QUESTION", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("QUESTION", _i0))
            return "ID_QUESTION", False, _i0, _i0, children


    def STAR(self, _s, _i0):
        result = _h.Literal("*")(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_STAR", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("STAR", _i0))
            return "ID_STAR", False, _i0, _i0, children


    def PLUS(self, _s, _i0):
        result = _h.Literal("+")(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_PLUS", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("PLUS", _i0))
            return "ID_PLUS", False, _i0, _i0, children


    def DOT(self, _s, _i0):
        result = _h.Literal(".")(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_DOT", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("DOT", _i0))
            return "ID_DOT", False, _i0, _i0, children


    def Spacing(self, _s, _i0):
        result = _h.ZeroOrMore(_h.Expression([self.Space, self.Comment]))(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Spacing", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Spacing", _i0))
            return "ID_Spacing", False, _i0, _i0, children


    def Comment(self, _s, _i0):
        result = _h.Sequence([_h.Literal("#"), _h.ZeroOrMore(_h.Sequence([_h.NOT(self.EndOfLine), _h.Dot()])), self.EndOfLine])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Comment", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Comment", _i0))
            return "ID_Comment", False, _i0, _i0, children


    def Space(self, _s, _i0):
        result = _h.Expression([_h.Literal(" "), _h.Literal("\t"), self.EndOfLine])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            return "ID_Space", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("Space", _i0))
            return "ID_Space", False, _i0, _i0, children


    def EndOfLine(self, _s, _i0):
        result = _h.Expression([_h.Sequence([_h.Literal("\r"), _h.ZeroOrOne(_h.Literal("\n"))]), _h.Literal("\n")])(_s, _i0)
        name, _m, _i0, _i, children = result
        if _m:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._newlines.append(_i0)
            return "ID_EndOfLine", True, _i0, _i, children
        else:
            if self._stack and _i0 > self._stack[0][1]:
                self._stack = []
            self._stack.append(("EndOfLine", _i0))
            return "ID_EndOfLine", False, _i0, _i0, children







class Processor(object):
    def __init__(self, s, handlers):
        self.s = s
        self.handlers = handlers

    def process(self, node):
        if node:
            if isinstance(node, ast_helpers.AST):
                self.process_ast(node)
            elif isinstance(node, list):
                self.process_list(node)
            elif isinstance(node, str):
                pass
                # return self.process_string(node)
            else:
                raise NotImplementedError
        else:
            pass
            # raise Exception

    def process_ast(self, node):
        if isinstance(node.children, list):
            for child in node.children:
                self.process(child)
            node.value = self.list_to_value(node.children)
        elif isinstance(node.children, str):
            node.value = node.children
        elif isinstance(node.children, ast_helpers.AST):
            self.process(node.children)
            node.value = node.children.value
        elif node.children is None:
            node.value = ""
        else:
            raise Exception
        if hasattr(self.handlers, node.name):
            self.handle_handler(node, getattr(self.handlers, node.name))

    def process_list(self, node):
        for _node in node:
            self.process(_node)

    def handle_handler(self, node, handler):
        handler(node)

    def list_to_value(self, _list):
        return ast_helpers.list_to_text(_list)

    # def process_default(self, node):
    #     return self.s[node[2]:node[3]]

class Compiler(object):
    def __init__(self, grammar, handlers):
        self.handlers = handlers
        self.processor = Processor(grammar, self.handlers)

    def compile(self, node):
        return self.processor.process(node)

def main():
    import io
    from optparse import OptionParser
    from pprint import pprint

    cli_parser = OptionParser(
        usage='usage: %prog [options] <grammar.file>'
        )
    cli_parser.add_option("-d", "--debug", action="store_true",
                      dest="debug", default=False,
                      help="Enable debugging")
    cli_parser.add_option("--handlers", dest="handlers",
                      help="Get handlers from FILE", metavar="FILE")
    (options, args) = cli_parser.parse_args()

    try:
        grammar_file = args[0]
    except IndexError:
        cli_parser.error("You have to supply a PEG <grammar.file>")
    grammar = io.open(grammar_file, "r", encoding="utf-8").read()
    #print grammar

    if not options.handlers:
        Handlers = object
    else:
        handlers = dict()
        exec(compile(open(options.handlers).read(), options.handlers, 'exec'),
             handlers)
        handlers["_DEBUG"] = options.debug
        if "Handlers" in handlers:
            Handlers = handlers["Handlers"]
        else:
            cli_parser.error("Your handlers file doesn't have a Handlers class")
    with keepcwd():
        dirname = os.path.dirname(grammar_file)
        if dirname:
            os.chdir(dirname)
        compiler = Compiler(grammar, Handlers())
        parser = Parser()
        result = parse(grammar, parser, compiler)
    if result[0]:
        print(result[1])
    else:
        cli_parser.error(result[1])

def parse(grammar, parser, compiler, filename=None):
    import sys
    import bisect
    import time

    parser = Parser()
    t1 = time.time()
    node = parser.Grammar(grammar, 0)
    name, isparsed, zero, length, ast = node
    t2 = time.time()
    if not isparsed or (length != len(grammar)):
        if filename is not None:
            message = 'The file "%s" is not a valid PEG file.'%filename
        else:
            message = "You have to supply a PEG <grammar.file>"
        _i = parser._stack[0][1]
        _name = parser._stack[0][0]
        _line_no = bisect.bisect(parser._newlines, _i) - 1
        if _line_no != -1:
            _newline = parser._newlines[_line_no]
        else:
            _newline = -1
        _next_newline = -1
        for _c in ('\r\n', '\n', '\r'):
            _c_i = grammar.find(_c, _newline+1)
            if  _c_i != -1:
                if _next_newline != -1:
                    if _c_i > _next_newline:
                        continue
                _next_newline = _c_i
        message += "Error while parsing line %d:\n"%(_line_no+1)
        if _next_newline != -1:
            _line = grammar[_newline+1:_next_newline]
        else:
            _line = grammar[_newline+1:]
        message += '%s\n'%_line
        _pointing_line = " "*(_i - _newline - 1) + "^\n"
        message += _pointing_line
        #message += repr(parser._stack) + "\n"
        message += "Traceback:\n"
        for _name, _j in reversed(parser._stack):
            if _j < _i:
                pass
                #message += "%s, %d\n"%(_name, _j)
            if _j == _i:
                if _i == len(grammar):
                    got = "EOF"
                else:
                    got = '"%s"'%grammar[_i]
                message += 'Expected %s at %d (%d, %d), got %s\n\n'%(_name, _i, _line_no + 1, (_i - _newline - 1), got)
                break
        return (False, message)
    else:
        t3 = time.time()
        node = ast_helpers.helper_remover(node)
        node = ast_helpers.node2ast(node)
        compiler.compile(node)
        result = node.value
        t4 = time.time()
        t5 = time.time()
        # print "The parsing took", str(t2-t1), "seconds."
        # print "Removing the helpers took", str(t4-t3), "seconds."
        # print "Converting to an AST object took", str(t5-t4), "seconds."
        # print "ast = ",
        # pprint(node)
        # result = ast_helpers.ast2dot(node)
        # pprint (ast)
        return (True, result)

if __name__ == '__main__':
    import pdb
    main()
    # import profile
    # profile.run('main()')

