#!/usr/bin/env python
import os
import sys
import time
import subprocess
from pprint import pprint

def myprint(text):
    print(text)

def ptime():
    return time.strftime("%Y-%m-%d, %H:%M:%S ", time.localtime())

def call_with_info(eval_text, globals=None, locals=None):
    start_time = ptime()
    myprint("Evaluating: %s, at %s"%(eval_text, start_time))
    try:
        result = eval(eval_text, globals, locals)
    except Exception as e:
        myprint(e)
        myprint("Error! Called on %s, broke on %s"%(start_time, ptime()))
        myprint("---------------------------------")
        raise
    else:
        print(result)
        myprint("Sucess! Called on %s, finished on %s"%(start_time, ptime()))
        myprint("---------------------------------")
        return result

def call(command):
    call_with_info("_call(%s)"%repr(command))

def _call(command):
    proc = subprocess.Popen(
                command,
                shell=True,
                universal_newlines=True,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                )
    (stdout, stderr) = proc.communicate()
    if proc.returncode != 0:
        e = "There was an error with process \"%s\". Return code is %d"%(command, proc.returncode)
        e += stderr
        print(stdout)
        raise Exception(e)
    return stdout

def diff_dict(dict1, dict2):
    diff = []
    if set(dict1) != set(dict2):
        raise Exception("Dictionaries must have the same keys.")
    for key in dict1:
        if dict1[key] != dict2[key]:
            diff.append(key)
    return diff

def get_times(fnames):
    times = {}
    for fname in fnames:
        if os.path.isfile(fname):
            times[fname] = os.stat(fname).st_mtime
        elif os.path.isdir(fname):
            times[fname] = get_times([os.path.join(fname, sub_fname) for sub_fname in os.listdir(fname)])
        else:
            pass
    return times

def watch(fnames, eval_text, globals=None, locals=None):
    old_times = get_times(fnames)
    myprint("Watching files %s"%str(fnames))
    while True:
        time.sleep(.1)
        new_times = get_times(fnames)
        if old_times != new_times:
            myprint("---------------------------------")
            myprint("Restarting... The following files have changed: %s"%str(diff_dict(old_times, new_times)))
            try:
                print(call_with_info(eval_text, globals, locals))
            except:
                pass
            old_times = new_times
            myprint("Watching files %s"%str(fnames))

def main():
    from optparse import OptionParser

    cli_parser = OptionParser(
        usage='usage: %prog [options] FILE1 FILE2 ...'
        )
    cli_parser.add_option("-c", "--command", dest="command",
                      help="execute COMMAND", metavar="COMMAND")
    cli_parser.add_option("-f", "--fake", action="store_true",
                      dest="fake", default=False,
                      help="Call the command only once and exit.")
    (options, args) = cli_parser.parse_args()

    if not args:
        cli_parser.error("You have to supply at least one FILE")
    if not options.command:
        cli_parser.error("You have to supply a COMMAND to run")
    if options.fake:
        myprint("Running the command only once.")
        myprint(ptime())
        myprint("---------------------------------")
        _call(options.command)
    else:
        eval_text = "_call(%s)"%repr(options.command)
        try:
            call_with_info(eval_text)
        except:
            pass
        watch(args, eval_text)

if __name__ == "__main__":
    main()
