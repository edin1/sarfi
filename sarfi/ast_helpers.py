import sys
import os

ID = 0

class keepcwd(object):
    def __enter__(self):
        self.curdir = os.getcwd()
        return self.curdir
    def __exit__(self, type, value, traceback):
        os.chdir(self.curdir)

def list_to_text(_list):
    text = ""
    for child in _list:
        if isinstance(child, list):
            text += list_to_text(child)
        elif isinstance(child, AST):
            text += child.value
        elif isinstance(child, str):
            text += child
        else:
            pass
            # raise NotImplementedError
    return text

def node2ast(node):
    if node is None or isinstance(node, str):
        return node
    elif isinstance(node, tuple):
        return AST(node)
    elif isinstance(node, list):
        return [node2ast(i) for i in node]
    else:
        # print type(node), node
        raise NotImplementedError

class AST(object):
    def __init__(self, node): # Node has to be tuple
        self.populate(node)
        self.value = None

    def populate(self, node): # node has to be a tuple
        self.name = node[0]
        self.m = node[1]
        self.i0 = node[2]
        self.i1 = node[3]
        if isinstance(node[4], list):
            self.children = [node2ast(i) for i in node[4]]
        else:
            self.children = node2ast(node[4])
        self._tuple = node

    # def __getitem__(self, index):
    #     node = self._tuple
    #     return (node[0], node[1], node[2], node[3], self.children)[index]

    def __contains__(self, name):
        if self.children is None or ininstance(self.children, str):
            return False
        for child in self.children:
            if child.name == name:
                return True
        return False

    def __repr__(self):
        if self.value is None:
            return repr(self._tuple)
        else:
            return "AST of: " + repr(self.value)

def helper_remover(node):
    if node is None or isinstance(node, str):
        return node
    elif isinstance(node, (tuple, AST)):
        if not node[1]: # This should? remove children of H_Not nodes
            return None
        if node[0].startswith("ID_"): # Identifier node
            node_name = node[0][3:]
            return node_name, node[1], node[2], node[3], helper_remover(node[4])
        else: # Helpers node
            return helper_remover(node[4])
        raise Exception # This shouldn't get called ever!
    elif isinstance(node, list):
        _list = []
        for elem in node:
            new_elem = helper_remover(elem)
            _list.append(new_elem)
        return cleanup_list(_list)
    else:
        raise NotImplementedError

def cleanup_list(_list):
    # try:
    #     text = "".join(_list)
    # except TypeError:
    #     pass
    # else:
    #     return text
    return _list

def ast2dot(node):
    text = """digraph ast {
ratio = "auto";
mincross = 2.0;
"""
    text += node2gv(node)[1]
    text += "\n}\n"
    return text

def node2gv(node):
    global ID
    ID += 1
    INDENT = "    "
    if isinstance(node, AST):
        name = node.name + "_" + str(ID)
        text = '%s [label="%s", style=filled, fillcolor=lightblue];\n'%(name, node.name)
        if node.children is None:
            pass
        elif isinstance(node.children, AST):
            child_name, child_text = node2gv(node.children)
            text += "%s -> %s;\n%s"%(name, child_name, child_text)
        elif isinstance(node.children, list):
            text += list2gv(node.children, name)
        elif isinstance(node.children, str):
            child_name, child_text = node2gv(node.children)
            text += "%s -> %s;\n"%(name, child_name)
            text += child_text
        else:
            # print type(node.children), node.children
            raise NotImplementedError
    elif isinstance(node, str):
        name = "str_" + str(ID)
        text = '%s [label="\\"%s\\"", shape=box, style=filled, fillcolor=lightcoral];\n'%(name, gv_escape(node))
    elif node is None:
        name = "None" + "_" + str(ID)
        text = ""
    elif isinstance(node, list):
        name = "list_" + str(ID)
        text = "%s [shape=box];\n"%name
        text += list2gv(node, name)
    else:
        raise NotImplementedError
    return name, text

def gv_escape(s):
    for pair in (
        ("\n", "\\\\n"),
        ("\r", "\\\\r"),
        ("\t", "\\\\t"),
        ('"', r'\"'),
        ):
        s = s.replace(pair[0], pair[1])
    return s

def list2gv(list_, parent_name):
    text = ""
    text_children = ""
    for child in list_:
        child_name, child_text = node2gv(child)
        text += "%s -> %s;\n"%(parent_name, child_name)
        text_children += child_text
    text += text_children
    return text
